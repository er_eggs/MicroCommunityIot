/*
 * Copyright 2017-2020 吴学文 and java110 team.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.java110.charge.cmd.chargeMachine;

import com.alibaba.fastjson.JSONObject;
import com.java110.bean.ResultVo;
import com.java110.core.annotation.Java110Cmd;
import com.java110.core.cmd.Cmd;
import com.java110.core.cmd.CmdEvent;
import com.java110.core.context.ICmdDataFlowContext;
import com.java110.core.exception.CmdException;
import com.java110.core.utils.BeanConvertUtil;
import com.java110.doc.annotation.*;
import com.java110.dto.chargeMachine.ChargeMachineFactoryDto;
import com.java110.dto.chargeMachine.ChargeMachineFactorySpecDto;
import com.java110.intf.charge.IChargeMachineFactorySpecV1InnerServiceSMO;
import com.java110.intf.charge.IChargeMachineFactoryV1InnerServiceSMO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;


/**
 * 类表述：查询
 * 服务编码：chargeMachineFactory.listChargeMachineFactory
 * 请求路劲：/app/chargeMachineFactory.ListChargeMachineFactory
 * add by 吴学文 at 2023-03-07 23:53:12 mail: 928255095@qq.com
 * open source address: https://gitee.com/wuxw7/MicroCommunity
 * 官网：http://www.homecommunity.cn
 * 温馨提示：如果您对此文件进行修改 请不要删除原有作者及注释信息，请补充您的 修改的原因以及联系邮箱如下
 * // modify by 张三 at 2021-09-12 第10行在某种场景下存在某种bug 需要修复，注释10至20行 加入 20行至30行
 */

@Java110CmdDoc(title = "查询充电桩厂家",
        description = "用于外系统查询充电桩厂家",
        httpMethod = "get",
        url = "http://{ip}:{port}/iot/api/chargeMachine.listChargeMachineFactory",
        resource = "chargeDoc",
        author = "吴学文",
        serviceCode = "chargeMachine.listChargeMachineFactory",
        seq = 13
)

@Java110ParamsDoc(params = {
        @Java110ParamDoc(name = "page", type = "int", length = 11, remark = "页数"),
        @Java110ParamDoc(name = "row", type = "int", length = 11, remark = "行数"),
})

@Java110ResponseDoc(
        params = {
                @Java110ParamDoc(name = "code", type = "int", length = 11, defaultValue = "0", remark = "返回编号，0 成功 其他失败"),
                @Java110ParamDoc(name = "msg", type = "String", length = 250, defaultValue = "成功", remark = "描述"),
                @Java110ParamDoc(name = "data", type = "Array", remark = "有效数据"),
                @Java110ParamDoc(parentNodeName = "data", name = "beanImpl", type = "String", remark = "厂家处理类"),
                @Java110ParamDoc(parentNodeName = "data", name = "factoryName", type = "String", remark = "厂家名称"),
        }
)

@Java110ExampleDoc(
        reqBody = "http://{ip}:{port}/iot/api/chargeMachine.listChargeMachineFactory?page=1&row=10",
        resBody = "{'code':0,'msg':'成功','data':[{'beanImpl':'commonChargeMachineFactory','factoryName':'通用充电桩'}]}"
)
@Java110Cmd(serviceCode = "chargeMachine.listChargeMachineFactory")
public class ListChargeMachineFactoryCmd extends Cmd {

  private static Logger logger = LoggerFactory.getLogger(ListChargeMachineFactoryCmd.class);
    @Autowired
    private IChargeMachineFactoryV1InnerServiceSMO chargeMachineFactoryV1InnerServiceSMOImpl;

    @Autowired
    private IChargeMachineFactorySpecV1InnerServiceSMO chargeMachineFactorySpecV1InnerServiceSMOImpl;

    @Override
    public void validate(CmdEvent event, ICmdDataFlowContext cmdDataFlowContext, JSONObject reqJson) {
        super.validatePageInfo(reqJson);
    }

    @Override
    public void doCmd(CmdEvent event, ICmdDataFlowContext cmdDataFlowContext, JSONObject reqJson) throws CmdException {

           ChargeMachineFactoryDto chargeMachineFactoryDto = BeanConvertUtil.covertBean(reqJson, ChargeMachineFactoryDto.class);

           int count = chargeMachineFactoryV1InnerServiceSMOImpl.queryChargeMachineFactorysCount(chargeMachineFactoryDto);

           List<ChargeMachineFactoryDto> chargeMachineFactoryDtos = null;

           if (count > 0) {
               chargeMachineFactoryDtos = chargeMachineFactoryV1InnerServiceSMOImpl.queryChargeMachineFactorys(chargeMachineFactoryDto);
               freshSpecs(chargeMachineFactoryDtos);
           } else {
               chargeMachineFactoryDtos = new ArrayList<>();
           }

           ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) reqJson.getInteger("row")), count, chargeMachineFactoryDtos);

           ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

           cmdDataFlowContext.setResponseEntity(responseEntity);
    }

    /**
     * 刷入配置
     *
     * @param chargeMachineFactoryDtos
     */
    private void freshSpecs(List<ChargeMachineFactoryDto> chargeMachineFactoryDtos) {

        if (chargeMachineFactoryDtos == null || chargeMachineFactoryDtos.size() < 1) {
            return;
        }

        List<String> factoryIds = new ArrayList<>();
        for (ChargeMachineFactoryDto chargeMachineFactoryDto : chargeMachineFactoryDtos) {
            factoryIds.add(chargeMachineFactoryDto.getFactoryId());
        }

        ChargeMachineFactorySpecDto chargeMachineFactorySpecDto = new ChargeMachineFactorySpecDto();
        chargeMachineFactorySpecDto.setFactoryIds(factoryIds.toArray(new String[factoryIds.size()]));

        List<ChargeMachineFactorySpecDto> machineFactorySpecDtos = chargeMachineFactorySpecV1InnerServiceSMOImpl.queryChargeMachineFactorySpecs(chargeMachineFactorySpecDto);

        if (machineFactorySpecDtos == null || machineFactorySpecDtos.size() < 1) {
            return;
        }
        List<ChargeMachineFactorySpecDto> specs = null;
        for (ChargeMachineFactoryDto chargeMachineFactoryDto : chargeMachineFactoryDtos) {
            specs = new ArrayList<>();
            for (ChargeMachineFactorySpecDto tmpMeterMachineFactorySpecDto : machineFactorySpecDtos) {
                if (chargeMachineFactoryDto.getFactoryId().equals(tmpMeterMachineFactorySpecDto.getFactoryId())) {
                    specs.add(tmpMeterMachineFactorySpecDto);
                }
            }
            chargeMachineFactoryDto.setSpecs(specs);
        }
    }
}
